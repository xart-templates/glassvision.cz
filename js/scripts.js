jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	$('.page_header>.toggle').on('click',function(){
		$('body').toggleClass('menu_main-on');
	});

	$('.mod_search .toggle').on('click',function(){
		$('body').toggleClass('mod_search-on');
	});

	$('.mod_custom-slideshow').each(function(){
		var i = $('.item',this).length;
		if(i>1){
			$(this).append('<div class="pager"/><div class="arrows"><a class="prev"/><a class="next"/></div>');
			$('.items',this).cycle({
				slides: '.item',
				fx: 'scrollHorz',
				speed: 900,
				timeout: 8000,
				pauseOnHover: true,
				swipe: true,
				pager: $('.pager',this),
				prev: $('.prev',this),
				next: $('.next',this),
			});
		}
	});
	
	function slideshowHeight(){
		var windowHeight = $(window).height();
		$('.mod_custom-slideshow').height(windowHeight);
	}
	slideshowHeight();
	$(window).resize(function(){
		slideshowHeight();
	});

	$('.mod_custom-slideshow').each(function(){
		$('.item',this).each(function(){
			var img = $('figure img',this).attr('src');
			$(this).css({'background-image':'url("'+img+'")'});
			$('figure',this).hide();
		});
	});

	$('.detail_images .slideshow').each(function(){
		var i = $('li',this).length;
		if(i>1) {
			$('ul',this).addClass('thumbnails').clone().addClass('items').removeClass('thumbnails').prependTo(this);
			$('.thumbnails',this).wrap('<div class="thumbnails_wrap"/>');
			$('.thumbnails_wrap',this).prepend('<a class="prev icon-arr-up"/><a class="next icon-arr-down"/>');
			$('.thumbnails',this).cycle({
				slides: 'li',
				fx: 'carousel',
				allowWrap: false,
				carouselVisible: 3,
				carouselVertical: true,
				speed: 200,
				timeout: 0,
				swipe: true,
				prev: $('.prev',this),
				next: $('.next',this),
			});
			$('.items',this).cycle({
				slides: 'li',
				fx: 'scrollHorz',
				speed: 200,
				timeout: 0,
				swipe: true,
				pager: $('.thumbnails .cycle-carousel-wrap',this),
				pagerTemplate: '',
			});
		}
	});

	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();

	// selectBoxIt
	$('select:not([multiple]').selectBoxIt();

	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});

	// basictable
	$('table.normal').basictable({
		breakpoint: 9999999, // css media query
	});

	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>150){
			$('body').addClass('offset_150');
		} else {
			$('body').removeClass('offset_150');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});

	new Maplace({
		locations: contact_map,
		map_div: '#contact-map',
		controls_on_map: false
	}).Load();
});